const db = require('./db.js')
var ObjectId = require('mongodb').ObjectId;

exports.addNewUser = function(user, cb) {
  db.get().collection('users').insertOne({login: user.login, password: user.password}, function(err, result) {
    cb(err, result);
  })
}

exports.findUserByLogin = function(login, cb) {
  db.get().collection('users').findOne({login: login}, function(err, doc) {
    cb(err, doc);
  });
}

exports.getUserByLogin = function(login, cb) {
  db.get().collection('users').findOne({login: login}, function(err, doc) {
    cb(err, doc);
  })
}

exports.addNewMessage = function(message, cb) {
  db.get().collection('messages').insertOne({userId: ObjectId(message.userId), text: message.text, date: new Date()}, function(err, result) {
    cb(err, result);
  })
}

exports.allMessages = function(cb) {
  db.get().collection('messages').aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'userId',
        foreignField: '_id',
        as: 'user'
      }
    },
    {
      $project: {
        _id: 1,
        userId: 1,
        userLogin: {"$arrayElemAt": ['$user.login', 0]},
        text: 1,
        date: 1,
      }
    }
  ]).toArray(function(err, docs) {
    cb(err, docs);
  });
}


exports.addToOnline = function(socketId, user, cb) {
  db.get().collection('online').insertOne({_id: socketId, userId: ObjectId(user._id)}, function(err, result) {
    cb(err, result)
  })
}

exports.removeFromOnline = function(socketId, cb) {
  db.get().collection('online').deleteOne({_id: socketId}, function(err, result) {
    cb(err, result)
  });
}

exports.online = function(cb) {
  db.get().collection('online').aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'userId',
        foreignField: '_id',
        as: 'user'
      }
    },
    {
      $project: {
        _id: 1,
        userId: 1,
        userLogin: {"$arrayElemAt": ['$user.login', 0]}
      }
    }
  ]).toArray(function(err, docs) {
    cb(err, docs);
  });
}
