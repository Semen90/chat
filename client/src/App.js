import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom'
import createHistory from "history/createBrowserHistory";


import './App.css';
import './app.scss'
import { setToLocalStorage, getLocalStorage, clearLocalStorage } from './utils'
import Home from './components/Home/Home'
import Header from './components/Header/Header'
import Registration from './components/Registration/Registration'

const history = createHistory();

class App extends Component {

  
  state = {
    isAuth: false,
    user: undefined,
  }
  
  componentWillMount() {
    const userFromLocalStorage = getLocalStorage()
    if (userFromLocalStorage) this.setState({user: userFromLocalStorage, isAuth: true})
  }

  chatExit = () => {
    clearLocalStorage();
    this.setState({isAuth: false, user: undefined})
  }

  setAuthUser = (user) => {
    setToLocalStorage(user);
    this.setState({isAuth: true, user: user})
  }

  render() {
    return (
      <Router history={history}>
        <div>
          <Header isAuth={this.state.isAuth} user={this.state.user}/>
          <div className="main-content">
            <Switch>
              <Route exact path="/" render={(props) => <Home
                  setAuthUser={this.setAuthUser}
                  chatExit={this.chatExit}
                  user={this.state.user}
                  isAuth={this.state.isAuth}
                  {...props}/>} />
              {!this.state.isAuth ? <Route path="/registration" component={Registration} /> : null}
              
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
