export const setToLocalStorage = (user) => {
  localStorage.setItem('chat-user', JSON.stringify(user));
}

export const getLocalStorage = () => {
  return JSON.parse(localStorage.getItem('chat-user'));
}

export const clearLocalStorage = () => {
  localStorage.clear();
}

export const getTimeByDate = (date) => {
  const newDate = new Date(date);
  const hours = (newDate.getHours() < 10 ? '0' : '') + newDate.getHours();
  const minutes = (newDate.getMinutes() < 10 ? '0' : '') + newDate.getMinutes();
  return `${hours}:${minutes}`;
}

export const getFormatDateByDate = (date) => {
  const newDate = new Date(date);
  return `${newDate.getDate()}.${newDate.getMonth() + 1}.${newDate.getFullYear().toString().substr(-2)}`
}
