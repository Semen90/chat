import React, { Component } from 'react'
import axios from 'axios'

export default class Registration extends Component {

  state = {
    login: '',
    password: '',
    confirmPassword: '',
    error: undefined,
    onLoad: false,
    isRegistered: false
  }

  textInput = (e) => {
    const name = e.target.name;
    if (this.state.error) this.setState({error: undefined})
    if (this.state.isRegistered) this.setState({isRegistered: false})

    switch (name) {
      case 'login':
        this.setState({login: e.target.value})
        break;

      case 'password': 
        this.setState({password: e.target.value})
        break;

      case 'confirmPassword':
        this.setState({confirmPassword: e.target.value})
        break;

      default:
        break;
    }
  }

  sendNewUser = () => {
    const { login, password, confirmPassword } = this.state;
    
    if (login.length < 1 || password.length < 1 || confirmPassword.length < 1) 
      return this.setState({error: 'Заполните все поля'})

    if (password !== confirmPassword) return this.setState({error: 'Пароли не совпадают'})

    this.setState({onLoad: true})
    axios.post('/api/user', {login, password, confirmPassword})
    .then(response => {
      if (response.data.isNotAvailable) return this.setState({onLoad: false, error: 'Такой логин уже есть'})

      if (response.data.ok) return this.setState({login: '', password: '', confirmPassword: '', onLoad: false, isRegistered: true})

      return this.setState({onLoad: false})
    })
    .catch(err => {
      this.setState({error: err.response.data.error, onLoad: false})
    })
  }

  render() {
    return (
      <div className="inner">
        <div className="main-text t_center">Регистрация</div>
        {this.state.onLoad ? <div className="loading">Загрузка...</div>
          :
          <form className="main-form">
            {this.state.isRegistered ? <div className="succes-text">Вы зарегистрированы!</div> : null}            
            <input value={this.state.login} onChange={this.textInput} name="login" type="text" placeholder="Введите имя" className="main-input" />
            <input value={this.state.password} onChange={this.textInput} name="password" type="password" placeholder="Введите пароль" className="main-input" />
            <input value={this.state.confirmPassword} onChange={this.textInput} name="confirmPassword" type="password" placeholder="Подтвердите пароль" className="main-input" />
            {this.state.error ? <div className="form-error">{this.state.error}</div> : null}
            <button className="button" type="button" onClick={this.sendNewUser}>Отправить</button>
          </form>
        }        
      </div>
    )
  }
}
