import React, { Component } from 'react'
import axios from 'axios'

export default class Signin extends Component {
  state = {
    login: '',
    password: '',
    error: undefined,
    onLoad: false
  }

  textInput = (e) => {
    const name = e.target.name;
    if (this.state.error) this.setState({error: undefined})

    switch (name) {
      case 'login':
        this.setState({login: e.target.value})
        break;

      case 'password': 
        this.setState({password: e.target.value})
        break;

      default:
        break;
    }
  }

  sendAuth = () => {
    const { login, password } = this.state;

    if (login.length < 1 || password.length < 1) return this.setState({error: 'Заполните все поля'})

    this.setState({onLoad: true})
    axios.post('/api/login', {login, password})
    .then(response => {
      if (response.data) {
        this.setState({login: '', password:'', onLoad: false}, () => {
          this.props.setAuthUser(response.data);
        })
        return;
      }

      return this.setState({onLoad: false})
    })
    .catch(err => {
      console.error(err.response.data)
      this.setState({error: err.response.data.error, onLoad: false})
    })
  }

  render() {
    const { onLoad } = this.state;

    return (
      <div>
        <div className="main-text t_center">Введите логин и пароль, чтобы войти в чат</div>
        {onLoad ? <div className="loading">Загрузка...</div>
          :
          <form className="main-form">
            <input name="login" onChange={this.textInput} value={this.state.login} type="text" placeholder="Введите имя" className="main-input" />
            <input name="password" onChange={this.textInput} value={this.state.password} type="password" placeholder="Введите пароль" className="main-input" />
            {this.state.error ? <div className="form-error">{this.state.error}</div> : null}
            <button className="button" type="button" onClick={this.sendAuth}>Войти</button>
          </form>
        }
      </div>
    )
  }
}
