import React, { Component } from 'react'
import axios from 'axios'

import './chatContolls.scss'

export default class ChatContolls extends Component {

  state = {
    text: '',
    onLoad: false,
    error: undefined
  }

  onChange = (e) => {
    if (this.state.error) this.setState({error: undefined});

    this.setState({text: e.target.value})
  }

  sendMessage = (e) => {
    e.preventDefault();
    
    if (this.state.text === '') return this.setState({error: 'Введите сообщение'});
    
    if (this.state.onLoad) return;
    
    
    this.setState({onLoad: true})
    // this.props.socket.emit('send-message', {text: this.state.text, userId: this.props.user._id})
    
    axios.post('/api/message', {
      text: this.state.text,
      userId: this.props.user._id
    })
    .then(response => {
      if (response.data.ok) return this.setState({onLoad: false, text: '', error: undefined})

      this.setState({onLoad: false})
    })
    .catch(err => {
      if (!err.response.data.error) return this.setState({error: 'Сервер не отвечает', onLoad: false})

      return this.setState({error: err.response.data.error, onLoad: false})
    })
  }

  render() {
    return (
      <div className="chat-controlls">
        <form className="chat-controlls__top">
          <textarea className="chat-controlls__textarea" value={this.state.text} onChange={this.onChange}></textarea>
          <button className="chat-controlls__send" onClick={this.sendMessage} disabled={this.state.onLoad}>send</button>
        </form>
        {this.state.error ? <div className="form-error">{this.state.error}</div> : null}
      </div>
    )
  }
}
