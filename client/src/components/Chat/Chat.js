import React, { Component } from 'react'
import axios from 'axios'
import openSocket from 'socket.io-client';

import './chat.scss'
import ChatList from './ChatList/ChatList'
import ChatMessages from './ChatMessages/ChatMessages'
import ChatContolls from './ChatContolls/ChatContolls'


export default class Chat extends Component {

  state = {
    messages: [],
    online: [],
    socket: undefined
  }

  componentWillMount = () => {
    this.initMessages();
    this.initSocket();
    this.initOnline();
  }


  initOnline = () => {
    axios.get('/api/online')
    .then(response => {
      if (response.data) {
        this.setState({online: response.data})
      }
    })
    .catch(err => {
      console.error(err.response.data.error)
    })
  }

  initSocket = () => {
    if (!this.state.socket) {
      this.setState({socket: openSocket()}, () => {

        this.state.socket.on('get-user', () => {
          let filtered = this.state.online.filter(user => user.userId === this.props.user._id)
          
          if (filtered.length <= 0) {
            this.state.socket.emit('send-user', {user: this.props.user})
          }
        })

        this.state.socket.on('new-online', () => {
          console.log('ENTER new-online')
          this.initOnline();
        })

        this.state.socket.on('message-added', (newMessage) => {
          const newArr = this.state.messages;
          newArr.push(newMessage);
          this.setState({messages: newArr})
          this.initMessages();
        })
      })      
    }
  }

  componentDidMount = () => {
    
  }
  

  initMessages = () => {
    axios.get('/api/message')
    .then(response => {
      // return console.log(response.data)
      if (response.data) {
        this.setState({messages: response.data})
      }
    })
    .catch(err => {
      console.error(err.response.data.error)
    })
  }
  

  chatExit = () => {
    this.state.socket.disconnect();
    this.props.chatExit();
  }

  render() {
    return (
      <div>
        <div className="t_r">
          <button type="button" className="button --sm" onClick={this.chatExit}>Выйти</button>
        </div>
        <div className="chat">
          <div className="users-list-wrap">
            <div className="chat-header">Участники</div>
            {this.state.online.length < 1 ? <div>Загрузка...</div> : <ChatList online={this.state.online} />}            
          </div>
          <div className="chat__right">
            <ChatMessages messages={this.state.messages}/>
            <ChatContolls socket={this.state.socket} user={this.props.user}/>
          </div>
        </div>
      </div>
    )
  }
}
