import React, { Component } from 'react'

import './chatMessages.scss'
import Message from '../Message/Message'

export default class ChatMessages extends Component {
  
  renderMessages = () => {
    return this.props.messages.map((message, index) => <Message key={index} message={message} />)
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  }
  
  componentDidMount() {
    this.scrollToBottom();
  }
  
  componentDidUpdate() {
    this.scrollToBottom();    
  }

  componentWillReceiveProps() {
    this.scrollToBottom();
  }

  render() {
    return (
      <ul className="chat-messages">
        {this.renderMessages()}
        <li style={{ float:"left", clear: "both" , listStyle: 'none'}}
             ref={(el) => { this.messagesEnd = el; }}>
        </li>
      </ul>
    )
  }
}
