import React, { Component } from 'react'

import './chatList.scss'

export default class ChatList extends Component {
  renderUsers = () => {
    return this.props.online.map((userOnline, index) => 
      <li key={index} className="users-list__item">{userOnline.userLogin}</li>
    )
  }

  render() {
    console.log(this.props.online)
    return (
      <ul className="users-list">
        {this.renderUsers()}
      </ul>
    )
  }
}
