import React from 'react';
import { getTimeByDate, getFormatDateByDate } from '../../../utils'
import './message.scss'


const Message = (props) => {
  const { userLogin, text, date } = props.message;

  return (
    <div className="message-wrap">
      <div className="message">
        <div className="message__top">
          <div className="message__user">{userLogin}</div>
          <div className="message__datetime">
            <span className="message__time">{date ? getTimeByDate(date) : null} </span>
            <span className="message__date">{date ? getFormatDateByDate(date) : null}</span>
          </div>
        </div>
        <div className="message__text">{text}</div>
      </div>
    </div>
    
  )
}

export default Message;