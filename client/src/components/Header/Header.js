import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

import './header.scss'

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <div className="inner">
          <div className="nav">
            <NavLink to="/" className="nav-link" activeClassName="_active" exact>Главная</NavLink>
            {!this.props.isAuth ? <NavLink to="/registration" className="nav-link" activeClassName="_active">Регистрация</NavLink> : null}
            {this.props.user ? <div className="user-login">{this.props.user.login}</div>: null}
          </div>
        </div>
      </div>
    )
  }
}
