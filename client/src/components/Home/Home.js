import React, { Component } from 'react'
import Signin from '../Signin/Signin'
import Chat from '../Chat/Chat'

export default class Home extends Component {

  render() {
    return (
      <div className="inner">
        {this.props.isAuth ?
          <Chat user={this.props.user} socket={this.props.socket} chatExit={this.props.chatExit}/>
          :
          <Signin setAuthUser={this.props.setAuthUser}/>
        }
      </div>
    )
  }
}
