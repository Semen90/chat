const express = require('express');
const path = require('path');
const router = require('express').Router();
const bodyParser = require('body-parser');
const fallback = require('express-history-api-fallback')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const controllers = require('./controllers.js');
const controllersSocket = require('./controllersSocket.js');
const Models = require('./models.js');

const config = require('./config.js')
var publicPath = path.join(__dirname, 'client', 'build');
var db = require('./db');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next){
  req.io = io;
  next();
});

app.use(express.static(publicPath));
app.use(router);
app.use(fallback('index.html', { root: publicPath }));
app.set('port', process.env.PORT || 17321);

router.get('/', function(req, res) {
  res.sendFile(path.join(publicPath, 'index.html'));
});


// users
router.post('/api/user', controllers.addNewUser);
router.post('/api/login', controllers.login);


// messages
router.get('/api/message', controllers.allMessages);
router.post('/api/message', controllers.addNewMessage);


// online
router.get('/api/online', controllers.online);


// io
io.on('connection', function(client) {
  console.log('user connected', client.id)

  client.emit('get-user');

  client.on('send-user', function(user) {
    Models.addToOnline(client.id, user.user, function(err, result) {
      io.emit('new-online');
    })
  });

  client.on('disconnect', function() {
    Models.removeFromOnline(client.id, function(err, result) {
      console.log('disconnect', client.id)
      io.emit('new-online');
    })
  });
})


db.connect(config.database, config.databaseName, function(err){
  if (err) {
    return console.log(err);
  }
  
  console.log('Connected to DB');

  io.listen(app.listen(app.get('port'), function() {
    console.log('Server http://localhost:' + app.get('port'));
  }));
  
});