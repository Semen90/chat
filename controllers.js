const Models = require('./models.js')


//  USERS *************************************
exports.addNewUser = function(req, res) {
  const user = req.body;

  findUserByLogin(user.login, function(err, doc) {
    if (err) return res.status(500).send({error: 'Ошибка на сервере. Попробуйте позже'});

    if (doc) return res.send({isNotAvailable: true});

    Models.addNewUser(user, function(err, result) {
      if (err) return res.status(500).send({error: 'Ошибка на сервере. Попробуйте позже'});
      
      res.send(result)
    })
  })
}

exports.login = function(req, res) {
  const user = req.body;

  Models.getUserByLogin(user.login, function(err, doc) {
    if (err) return res.status(500).send({error: 'Ошибка на сервере. Попробуйте позже'});
    
    if (!doc) return res.status(401).send({error: 'Неверный логин или пароль'});
    
    if (user.password !== doc.password) return res.status(401).send({error: 'Неверный логин или пароль'});
    
    res.send({_id: doc._id, login: doc.login});
  })
}

//  MESSAGES *************************************
exports.allMessages = function(req, res) {
  Models.allMessages(function(err, docs) {
    if (err) return res.status(500).send({error: 'Ошибка на сервере. Попробуйте позже'});

    res.send(docs);
  })
}

exports.addNewMessage = function(req, res) {
  const message = req.body;

  Models.addNewMessage(message, function(err, result) {
    if (err) return res.status(500).send({error: 'Ошибка на сервере. Попробуйте позже'});

    req.io.emit('message-added', result.ops);
    res.send(result);
  })
}

function findUserByLogin(login, cb) {
  Models.findUserByLogin(login, function(err, doc) {
    cb(err, doc);
  })
}


// ONLINE

exports.online = function(req, res) {
  Models.online(function(err, docs) {
    if (err) return res.status(500).send({error: 'Ошибка на сервере. Попробуйте позже'});

    res.send(docs);
  })
}